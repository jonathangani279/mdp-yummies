package com.project.yummies;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Adapter_rv_bookmark extends RecyclerView.Adapter<Adapter_rv_bookmark.GridViewHolder> {
    ArrayList<Item> arrItem = new ArrayList<>();
    public ViewGroup parent;
    private static RVRecipesClickListener mylistener;

    public Adapter_rv_bookmark(ArrayList<Item> arrItem, RVRecipesClickListener RVRecipe) {
        this.arrItem = arrItem;
        mylistener= RVRecipe;
    }

    public Adapter_rv_bookmark(ArrayList<Item> arrItem) { this.arrItem= arrItem; }

    @NonNull
    @Override
    public GridViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_bookmark, parent, false);
        this.parent = parent;
        return new GridViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull GridViewHolder holder, int position) {
        Item item = arrItem.get(position);
        Glide.with(parent.getContext())
                .load(item.getImgurl())
                .apply(new RequestOptions().override(400,400))
                .into(holder.img);
        holder.txt.setText(item.getTitle());
    }

    @Override
    public int getItemCount() {
        return arrItem.size();
    }

    public class GridViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView txt;
        public GridViewHolder(@NonNull View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.rv_bookmark_img_display);
            txt = itemView.findViewById(R.id.rv_bookmark_txt_title);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mylistener != null) {
                        mylistener.recipesClickListener(view, GridViewHolder.this.getLayoutPosition());
                    }
                }
            });
        }
    }
}
