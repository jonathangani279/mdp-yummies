package com.project.yummies;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.AdaptiveIconDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Adapter_rv_home extends RecyclerView.Adapter<Adapter_rv_home.ListViewHolder> {
    ArrayList<Item> arrItem;
    ViewGroup parent;
    ArrayList<ImageView> arrBintang;
    private static RVRecipesClickListener mylistener;

    public Adapter_rv_home(ArrayList<Item> arrItem) {
        this.arrItem = arrItem;
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_home, parent, false);
        this.parent = parent;
        return new ListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ListViewHolder holder, final int position) {
        final Item item = arrItem.get(position);
        holder.tvTitle.setText(item.getTitle());
        holder.tvReady.setText("Ready in: " + item.getReady() + " minutes");
        holder.tvPortion.setText("Portion: " + item.getPortion());
        arrBintang = new ArrayList<>();
        arrBintang.add(holder.imgStar1);
        arrBintang.add(holder.imgStar2);
        arrBintang.add(holder.imgStar3);
        arrBintang.add(holder.imgStar4);
        arrBintang.add(holder.imgStar5);
        int batas = item.getRated();
        for (int i=0; i<batas; i++){
            arrBintang.get(i).setImageResource(R.drawable.ic_star_black_24dp);
        }
        for (int i=batas; i<5; i++){
            arrBintang.get(i).setImageResource(R.drawable.ic_star_border);
        }
        holder.tvRating.setText(item.getRating() + "");
        if(item.isBookmarked())
            holder.imgBookmark.setImageResource(R.drawable.ic_bookmark);
        else
            holder.imgBookmark.setImageResource(R.drawable.ic_bookmark_border);
        if(item.isLiked())
            holder.imgLike.setImageResource(R.drawable.ic_favorite);
        else
            holder.imgLike.setImageResource(R.drawable.ic_favorite_border);
        Glide.with(parent.getContext())
                .load(item.getImgurl())
                .apply(new RequestOptions().override(200,200))
                .into(holder.imgItem);
        holder.imgLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(item.isLiked()){
                    final String userid = LoginActivity.account.getId();
                    final String url = "http://furniterior.000webhostapp.com/mdp/ws/delete_like.php";
                    final StringRequest stringRequest = new StringRequest(
                            Request.Method.POST,
                            url,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                }
                            }
                    ) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("userid", userid);
                            params.put("item", arrItem.get(position).getJson());

                            return params;
                        }
                    };

                    RequestQueue requestQueue = Volley.newRequestQueue(parent.getContext());
                    requestQueue.add(stringRequest);
                    item.setLiked(false);
                    holder.imgLike.setImageResource(R.drawable.ic_favorite_border);
                }else{
                    final String userid = LoginActivity.account.getId();
                    final String url = "http://furniterior.000webhostapp.com/mdp/ws/insert_like.php";
                    final StringRequest stringRequest = new StringRequest(
                            Request.Method.POST,
                            url,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                }
                            }
                    ) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("userid", userid);
                            params.put("itemid", arrItem.get(position).getJson());

                            return params;
                        }
                    };

                    RequestQueue requestQueue = Volley.newRequestQueue(parent.getContext());
                    requestQueue.add(stringRequest);
                    item.setLiked(true);
                    holder.imgLike.setImageResource(R.drawable.ic_favorite);
                }
            }
        });
        holder.imgBookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(item.isBookmarked()){
                    final String userid = LoginActivity.account.getId();
                    final String url = "http://furniterior.000webhostapp.com/mdp/ws/delete_bookmark.php";
                    final StringRequest stringRequest = new StringRequest(
                            Request.Method.POST,
                            url,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                }
                            }
                    ) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("userid", userid);
                            params.put("item", arrItem.get(position).getJson());

                            return params;
                        }
                    };

                    RequestQueue requestQueue = Volley.newRequestQueue(parent.getContext());
                    requestQueue.add(stringRequest);
                    item.setBookmarked(false);
                    holder.imgBookmark.setImageResource(R.drawable.ic_bookmark_border);
                }else{
                    final String userid = LoginActivity.account.getId();
                    final String url = "http://furniterior.000webhostapp.com/mdp/ws/insert_bookmark.php";
                    System.out.println(arrItem.get(position).getJson());
                    final StringRequest stringRequest = new StringRequest(
                            Request.Method.POST,
                            url,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                }
                            }
                    ) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("userid", userid);
                            params.put("itemid", arrItem.get(position).getJson());

                            return params;
                        }
                    };

                    RequestQueue requestQueue = Volley.newRequestQueue(parent.getContext());
                    requestQueue.add(stringRequest);
                    item.setBookmarked(true);
                    holder.imgBookmark.setImageResource(R.drawable.ic_bookmark);
                }
            }
        });


        holder.imgStar5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.imgStar1.setImageResource(R.drawable.ic_star_black_24dp);
                holder.imgStar2.setImageResource(R.drawable.ic_star_black_24dp);
                holder.imgStar3.setImageResource(R.drawable.ic_star_black_24dp);
                holder.imgStar4.setImageResource(R.drawable.ic_star_black_24dp);
                holder.imgStar5.setImageResource(R.drawable.ic_star_black_24dp);
                item.setRated(5);
                final String userid = LoginActivity.account.getId();
                final String url = "http://furniterior.000webhostapp.com/mdp/ws/insert_rate.php";
                final StringRequest stringRequest = new StringRequest(
                        Request.Method.POST,
                        url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                /*for (int i = 0; i< 5; i++){
                                    arrBintang.get(i).setImageResource(R.drawable.ic_star_black_24dp);
                                }*/
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("userid", userid);
                        params.put("itemid", arrItem.get(position).getJson());
                        params.put("rate", "5");
                        return params;
                    }
                };

                RequestQueue requestQueue = Volley.newRequestQueue(parent.getContext());
                requestQueue.add(stringRequest);
            }
        });
        holder.imgStar4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.imgStar1.setImageResource(R.drawable.ic_star_black_24dp);
                holder.imgStar2.setImageResource(R.drawable.ic_star_black_24dp);
                holder.imgStar3.setImageResource(R.drawable.ic_star_black_24dp);
                holder.imgStar4.setImageResource(R.drawable.ic_star_black_24dp);
                holder.imgStar5.setImageResource(R.drawable.ic_star_border);
                item.setRated(4);
                final String userid = LoginActivity.account.getId();
                final String url = "http://furniterior.000webhostapp.com/mdp/ws/insert_rate.php";
                final StringRequest stringRequest = new StringRequest(
                        Request.Method.POST,
                        url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("userid", userid);
                        params.put("itemid", arrItem.get(position).getJson());
                        params.put("rate", "4");
                        return params;
                    }
                };

                RequestQueue requestQueue = Volley.newRequestQueue(parent.getContext());
                requestQueue.add(stringRequest);
            }
        });
        holder.imgStar3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.imgStar1.setImageResource(R.drawable.ic_star_black_24dp);
                holder.imgStar2.setImageResource(R.drawable.ic_star_black_24dp);
                holder.imgStar3.setImageResource(R.drawable.ic_star_black_24dp);
                holder.imgStar4.setImageResource(R.drawable.ic_star_border);
                holder.imgStar5.setImageResource(R.drawable.ic_star_border);
                item.setRated(3);
                final String userid = LoginActivity.account.getId();
                final String url = "http://furniterior.000webhostapp.com/mdp/ws/insert_rate.php";
                final StringRequest stringRequest = new StringRequest(
                        Request.Method.POST,
                        url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("userid", userid);
                        params.put("itemid", arrItem.get(position).getJson());
                        params.put("rate", "3");
                        return params;
                    }
                };

                RequestQueue requestQueue = Volley.newRequestQueue(parent.getContext());
                requestQueue.add(stringRequest);
            }
        });
        holder.imgStar2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.imgStar1.setImageResource(R.drawable.ic_star_black_24dp);
                holder.imgStar2.setImageResource(R.drawable.ic_star_black_24dp);
                holder.imgStar3.setImageResource(R.drawable.ic_star_border);
                holder.imgStar4.setImageResource(R.drawable.ic_star_border);
                holder.imgStar5.setImageResource(R.drawable.ic_star_border);
                item.setRated(2);
                final String userid = LoginActivity.account.getId();
                final String url = "http://furniterior.000webhostapp.com/mdp/ws/insert_rate.php";
                final StringRequest stringRequest = new StringRequest(
                        Request.Method.POST,
                        url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("userid", userid);
                        params.put("itemid", arrItem.get(position).getJson());
                        params.put("rate", "2");
                        return params;
                    }
                };

                RequestQueue requestQueue = Volley.newRequestQueue(parent.getContext());
                requestQueue.add(stringRequest);
            }
        });
        holder.imgStar1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.imgStar1.setImageResource(R.drawable.ic_star_black_24dp);
                holder.imgStar2.setImageResource(R.drawable.ic_star_border);
                holder.imgStar3.setImageResource(R.drawable.ic_star_border);
                holder.imgStar4.setImageResource(R.drawable.ic_star_border);
                holder.imgStar5.setImageResource(R.drawable.ic_star_border);
                item.setRated(1);
                final String userid = LoginActivity.account.getId();
                final String url = "http://furniterior.000webhostapp.com/mdp/ws/insert_rate.php";
                final StringRequest stringRequest = new StringRequest(
                        Request.Method.POST,
                        url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("userid", userid);
                        params.put("itemid", arrItem.get(position).getJson());
                        params.put("rate", "1");
                        return params;
                    }
                };

                RequestQueue requestQueue = Volley.newRequestQueue(parent.getContext());
                requestQueue.add(stringRequest);
            }
        });
        holder.imgShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(parent.getContext(), ShareActivity.class);
                intent.putExtra("item", item);
                parent.getContext().startActivity(intent);
            }
        });

    }
    public Adapter_rv_home(ArrayList<Item> arrItem,RVRecipesClickListener RVRecipes) {
        this.arrItem = arrItem;
        mylistener=RVRecipes;
    }

    @Override
    public int getItemCount() {
        return arrItem.size();
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {
        ImageView imgItem, imgStar1, imgStar2, imgStar3, imgStar4, imgStar5, imgLike, imgShare, imgBookmark;
        TextView tvTitle, tvReady, tvPortion, tvRating;
        public ListViewHolder(@NonNull View itemView) {
            super(itemView);
            imgItem = itemView.findViewById(R.id.rv_home_img_item);
            imgStar1 = itemView.findViewById(R.id.rv_home_img_star_1);
            imgStar2 = itemView.findViewById(R.id.rv_home_img_star_2);
            imgStar3 = itemView.findViewById(R.id.rv_home_img_star_3);
            imgStar4 = itemView.findViewById(R.id.rv_home_img_star_4);
            imgStar5 = itemView.findViewById(R.id.rv_home_img_star_5);
            imgLike = itemView.findViewById(R.id.rv_home_img_like);
            imgShare = itemView.findViewById(R.id.rv_home_img_share);
            imgBookmark = itemView.findViewById(R.id.rv_home_img_bookmark);
            tvTitle = itemView.findViewById(R.id.rv_home_txt_title);
            tvReady = itemView.findViewById(R.id.rv_home_txt_ready);
            tvPortion = itemView.findViewById(R.id.rv_home_txt_portion);
            tvRating = itemView.findViewById(R.id.rv_home_rating);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mylistener.recipesClickListener(v,ListViewHolder.this.getLayoutPosition());
                }
            });
        }
    }
}
