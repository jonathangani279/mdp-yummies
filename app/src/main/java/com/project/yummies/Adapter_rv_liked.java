package com.project.yummies;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import java.util.ArrayList;

public class Adapter_rv_liked extends RecyclerView.Adapter<Adapter_rv_liked.LikedViewHolder> {
    ViewGroup parent;
    ArrayList<Item> likedFood;
    private static RVRecipesClickListener mylistener;

    public Adapter_rv_liked(ArrayList<Item> likedFood, RVRecipesClickListener RVRecipes) {
        this.likedFood = likedFood;
        mylistener= RVRecipes;
    }

    @NonNull
    @Override
    public Adapter_rv_liked.LikedViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_liked, parent, false);
        this.parent = parent;
        return new Adapter_rv_liked.LikedViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull Adapter_rv_liked.LikedViewHolder holder, int position) {
        Item food= likedFood.get(position);

        Glide.with(parent.getContext())
                .load(food.getImgurl())
                .into(holder.ivFood);

        holder.tvName.setText(food.getTitle());
    }

    @Override
    public int getItemCount() {
        return this.likedFood.size();
    }

    public class LikedViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivFood;
        private TextView tvName;
        public LikedViewHolder(@NonNull View itemView) {
            super(itemView);
            this.ivFood= itemView.findViewById(R.id.rv_liked_img_display);
            this.tvName= itemView.findViewById(R.id.rv_liked_txt_name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mylistener.recipesClickListener(v, LikedViewHolder.this.getLayoutPosition());
                }
            });
        }
    }
}
