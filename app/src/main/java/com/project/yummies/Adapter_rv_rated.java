package com.project.yummies;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class Adapter_rv_rated extends RecyclerView.Adapter<Adapter_rv_rated.RatedViewHolder> {
    private ViewGroup parent;
    private ArrayList<Item> ratedFood;
    private static RVRecipesClickListener mylistener;

    public Adapter_rv_rated(ArrayList<Item> ratedFood, RVRecipesClickListener listener) {
        this.ratedFood = ratedFood;
        mylistener= listener;
    }

    @NonNull
    @Override
    public Adapter_rv_rated.RatedViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_rated, parent, false);
        this.parent = parent;
        return new RatedViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Adapter_rv_rated.RatedViewHolder holder, int position) {
        Item food= ratedFood.get(position);

        Glide.with(parent.getContext())
                .load(food.getImgurl())
                .into(holder.ivFood);

        holder.tvName.setText(food.getTitle());
    }

    @Override
    public int getItemCount() {
        return this.ratedFood.size();
    }

    public class RatedViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivFood;
        private TextView tvName;

        public RatedViewHolder(@NonNull View itemView) {
            super(itemView);
            this.ivFood= itemView.findViewById(R.id.rv_rated_img_display);
            this.tvName= itemView.findViewById(R.id.rv_rated_txt_name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mylistener.recipesClickListener(view, RatedViewHolder.this.getLayoutPosition());
                }
            });
        }
    }
}
