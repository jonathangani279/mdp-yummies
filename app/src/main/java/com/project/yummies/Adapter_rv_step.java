package com.project.yummies;

import android.os.IInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class Adapter_rv_step extends RecyclerView.Adapter<Adapter_rv_step.ListViewHolder> {

//    ArrayList<Item> arrItem;
    ArrayList<String> arrStep;

//    public Adapter_rv_step(ArrayList<Item> arrItem) {
//        this.arrItem = arrItem;
//    }

    public Adapter_rv_step(ArrayList<String> arrStep) {
        this.arrStep = arrStep;
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_step, parent, false);
        return new Adapter_rv_step.ListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder holder, int position) {
//        final Item item = arrItem.get(position);
//        String tampung="";
//        for (int i = 0; i < item.getStep().size(); i++) {
//            tampung=tampung+(i+1)+". "+item.getStep().get(i)+"\n\n";
//        }
//        holder.isi.setText(tampung);
            holder.isi.setText((position+1)+". "+arrStep.get(position));
    }

    @Override
    public int getItemCount() {
//        return arrItem.size();
        return  arrStep.size();
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {
        TextView isi;
        public ListViewHolder(@NonNull View itemView) {
            super(itemView);
            isi=itemView.findViewById(R.id.tvIsiStep);
        }
    }
}
