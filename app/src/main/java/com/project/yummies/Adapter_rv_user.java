package com.project.yummies;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class Adapter_rv_user extends RecyclerView.Adapter<Adapter_rv_user.ListViewHolder> {
    ArrayList<User> arrUser;
    ViewGroup parent;
    public Adapter_rv_user(ArrayList<User> arrUser) {
        this.arrUser = arrUser;
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.parent = parent;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_user, parent, false);
        return new ListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder holder, int position) {
        final User u = arrUser.get(position);
        Glide.with(parent.getContext())
                .load(u.getUser_profile_picture())
                .apply(new RequestOptions().override(200,200))
                .into(holder.img);
        holder.t1.setText(u.getUser_nama_depan() + " " + u.getUser_nama_belakang());
        holder.t2.setText(u.getUser_username());
        holder.btn1.setText(u.getTulisan());
        if(u.getUser_id().equals(LoginActivity.account.getId()))
            holder.btn1.setVisibility(View.INVISIBLE);
        holder.btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button btn = (Button) view;
                if(btn.getText().toString().equalsIgnoreCase("follow")){
                    final String url = "http://furniterior.000webhostapp.com/mdp/ws/follow_user.php?userid="+LoginActivity.account.getId()+"&useridfollow="+u.getUser_id();
                    System.out.println(url);
                    StringRequest stringRequest = new StringRequest(
                            Request.Method.GET,
                            url,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                }
                            }
                    );

                    RequestQueue requestQueue = Volley.newRequestQueue(parent.getContext());
                    requestQueue.add(stringRequest);
                    btn.setText("unfollow");
                }else{
                    final String url = "http://furniterior.000webhostapp.com/mdp/ws/unfollow_user.php?userid="+LoginActivity.account.getId()+"&useridfollow="+u.getUser_id();
                    System.out.println(url);
                    StringRequest stringRequest = new StringRequest(
                            Request.Method.GET,
                            url,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                }
                            }
                    );

                    RequestQueue requestQueue = Volley.newRequestQueue(parent.getContext());
                    requestQueue.add(stringRequest);
                    btn.setText("Follow");
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrUser.size();
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {
        CircleImageView img;
        TextView t1,t2;
        Button btn1;
        public ListViewHolder(@NonNull View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.user_img_pp);
            t1 = itemView.findViewById(R.id.user_txt_name);
            t2 = itemView.findViewById(R.id.user_txt_username);
            btn1 = itemView.findViewById(R.id.user_btn_follow);
        }
    }
}
