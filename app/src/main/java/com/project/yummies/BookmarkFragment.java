package com.project.yummies;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class BookmarkFragment extends Fragment {
    ArrayList<Item> arrItem = new ArrayList<>();
    Adapter_rv_bookmark adapter;
    RecyclerView rv;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.fragment_bookmark, container, false);
        rv = v.findViewById(R.id.fragment_bookmark_rv);
        adapter = new Adapter_rv_bookmark(arrItem, new RVRecipesClickListener() {
            @Override
            public void recipesClickListener(View v, int posisi) {
                Item item = null;
                try {
                    item= new Item(new JSONObject(arrItem.get(posisi).getJson()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Intent i = new Intent(BookmarkFragment.this.getContext(),DetailResepActivity.class);
                i.putExtra("item",item);
                startActivity(i);
            }
        });
        loadBookmark();

        //adapter.setHasStableIds(true);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new GridLayoutManager(getContext(), 2));
        rv.setAdapter(adapter);

        return v;
    }
    
    private void loadBookmark() {
        String userid = LoginActivity.account.getId();
        final String url = "http://furniterior.000webhostapp.com/mdp/ws/get_bookmark.php?userid="+userid;
        arrItem.clear();
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray list = obj.getJSONArray("results");
                            for (int i = 0; i < list.length(); i++) {
                                JSONObject jsonItem = list.getJSONObject(i);
                                arrItem.add(new Item(jsonItem));
                            }
                            adapter.notifyDataSetChanged();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        );

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
