package com.project.yummies;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CommunityFragment extends Fragment {
    TabLayout tlCommunity;
    RecyclerView rvCommunity;
    Adapter_rv_user adapteruser;
    Adapter_rv_bookmark adapterbookmark;
    ArrayList<User> arrUser = new ArrayList<>();
    ArrayList<Item> arrItem = new ArrayList<>();
    MainActivity m;
    int position = 0;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        m = (MainActivity) getActivity();
        return inflater.inflate(R.layout.fragment_community, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getUser();
        getItem();
        tlCommunity = view.findViewById(R.id.tlCommunity);
        tlCommunity.addOnTabSelectedListener(onTabSelectedListener);
        rvCommunity = view.findViewById(R.id.rvCommunity);
        adapterbookmark = new Adapter_rv_bookmark(arrItem, new RVRecipesClickListener() {
            @Override
            public void recipesClickListener(View v, int posisi) {
                Item item = null;
                try {
                    item= new Item(new JSONObject(arrItem.get(posisi).getJson()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Intent i = new Intent(CommunityFragment.this.getContext(),DetailResepActivity.class);
                i.putExtra("item",item);
                startActivity(i);
            }
        });

        //adapterbookmark.setHasStableIds(true);
        rvCommunity.setHasFixedSize(true);
        rvCommunity.setLayoutManager(new GridLayoutManager(getContext(), 2));
        rvCommunity.setAdapter(adapterbookmark);

        //104082473564984161311
    }
    public TabLayout.OnTabSelectedListener onTabSelectedListener = new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            if(tab.getPosition()==0)
            {
                position = 0;
                adapterbookmark = new Adapter_rv_bookmark(arrItem, new RVRecipesClickListener() {
                    @Override
                    public void recipesClickListener(View v, int posisi) {
                        Item item = null;
                        try {
                            item= new Item(new JSONObject(arrItem.get(posisi).getJson()));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Intent i = new Intent(CommunityFragment.this.getContext(),DetailResepActivity.class);
                        i.putExtra("item",item);
                        startActivity(i);
                    }
                });

                //adapterbookmark.setHasStableIds(true);
                rvCommunity.setHasFixedSize(true);
                rvCommunity.setLayoutManager(new GridLayoutManager(getContext(), 2));
                rvCommunity.setAdapter(adapterbookmark);
            }
            else if(tab.getPosition()==1)
            {
                position=1;
                adapteruser = new Adapter_rv_user(arrUser);

                //adapteruser.setHasStableIds(true);
                rvCommunity.setHasFixedSize(true);
                rvCommunity.setLayoutManager(new LinearLayoutManager(getContext()));
                rvCommunity.setAdapter(adapteruser);
            }
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {
            if(tab.getPosition()==0)
            {
                position = 0;
                adapterbookmark = new Adapter_rv_bookmark(arrItem, new RVRecipesClickListener() {
                    @Override
                    public void recipesClickListener(View v, int posisi) {
                        Item item = null;
                        try {
                            item= new Item(new JSONObject(arrItem.get(posisi).getJson()));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Intent i = new Intent(CommunityFragment.this.getContext(),DetailResepActivity.class);
                        i.putExtra("item",item);
                        startActivity(i);
                    }
                });

                //adapterbookmark.setHasStableIds(true);
                rvCommunity.setHasFixedSize(true);
                rvCommunity.setLayoutManager(new GridLayoutManager(getContext(), 2));
                rvCommunity.setAdapter(adapterbookmark);
            }
            else if(tab.getPosition()==1)
            {
                position=1;
                adapteruser = new Adapter_rv_user(arrUser);

                //adapteruser.setHasStableIds(true);
                rvCommunity.setHasFixedSize(true);
                rvCommunity.setLayoutManager(new LinearLayoutManager(getContext()));
                rvCommunity.setAdapter(adapteruser);
            }
        }
    };

    public void getItem()
    {
        final String url = "http://furniterior.000webhostapp.com/mdp/ws/get_share.php?userid="+LoginActivity.account.getId();
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            arrItem.clear();
                            JSONObject obj = new JSONObject(response);
                            JSONArray list = obj.getJSONArray("results");
                            for (int i = 0; i< list.length(); i++){
                                JSONObject jsonItem = list.getJSONObject(i);
                                Item item = new Item(jsonItem);
                                arrItem.add(item);
                            }
                            adapterbookmark.notifyDataSetChanged();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        );

        RequestQueue requestQueue = Volley.newRequestQueue(m.getApplicationContext());
        requestQueue.add(stringRequest);
    }
    public void getUser()
    {
        final String url = "http://furniterior.000webhostapp.com/mdp/ws/get_following.php?userid="+LoginActivity.account.getId();
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray list = obj.getJSONArray("results");
                            arrUser.clear();
                            for (int i = 0; i < list.length(); i++) {
                                JSONObject jsonItem = list.getJSONObject(i);
                                arrUser.add(new User(jsonItem));
                            }
                            adapteruser.notifyDataSetChanged();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        );

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }


}

