package com.project.yummies;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DetailResepActivity extends AppCompatActivity {
    TextView namaresep,lingkaran,ready,portion;
    ImageView foto,bintang1,bintang2,bintang3,bintang4,bintang5,book,like;
    ArrayList<String>arrStep = new ArrayList<>();
    Adapter_rv_step adapterRvStep;
    RecyclerView rvstep;
    WebView webView;
    Item item;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_resep);

        namaresep=findViewById(R.id.tvNamaResep);
        foto=findViewById(R.id.ivFotoResep);
        lingkaran=findViewById(R.id.tvLingkaran);
        rvstep=findViewById(R.id.rvStep);
        ready=findViewById(R.id.tvReady);
        portion=findViewById(R.id.tvPortion);
        bintang1=findViewById(R.id.ivBintang1);
        bintang2=findViewById(R.id.ivBintang2);
        bintang3=findViewById(R.id.ivBintang3);
        bintang4=findViewById(R.id.ivBintang4);
        bintang5=findViewById(R.id.ivBintang5);
        book=findViewById(R.id.rv_home_img_bookmark);
        like=findViewById(R.id.rv_home_img_like);

        GradientDrawable drawable = new GradientDrawable();
        drawable.setShape(GradientDrawable.OVAL);
        drawable.setCornerRadius(100);
        drawable.setStroke(5, Color.WHITE);
        lingkaran.setBackground(drawable);

        GradientDrawable drawable2 = new GradientDrawable();
        drawable2.setShape(GradientDrawable.RECTANGLE);
        drawable2.setCornerRadius(20);
        drawable2.setColor(Color.WHITE);
        drawable2.setStroke(5, Color.BLACK);
        rvstep.setBackground(drawable2);

        Intent i = getIntent();
        item = (Item) i.getSerializableExtra("item");
        Glide.with(getApplicationContext())
                .load(item.getImgurl())
                .apply(new RequestOptions().override(200,200))
                .into(foto);
        namaresep.setText(item.getTitle());
        ready.setText("Ready in : "+item.getReady()+" Minutes");
        portion.setText("Portion : "+item.getPortion());

        arrStep=item.getStep();

        adapterRvStep = new Adapter_rv_step(arrStep);
        rvstep.setLayoutManager(new LinearLayoutManager(this));
        rvstep.setAdapter(adapterRvStep);

        if(item.getRated()==5){
            bintang1.setImageResource(R.drawable.ic_star_black_24dp);
            bintang2.setImageResource(R.drawable.ic_star_black_24dp);
            bintang3.setImageResource(R.drawable.ic_star_black_24dp);
            bintang4.setImageResource(R.drawable.ic_star_black_24dp);
            bintang5.setImageResource(R.drawable.ic_star_black_24dp);
        }
        else if(item.getRated()==4){
            bintang1.setImageResource(R.drawable.ic_star_black_24dp);
            bintang2.setImageResource(R.drawable.ic_star_black_24dp);
            bintang3.setImageResource(R.drawable.ic_star_black_24dp);
            bintang4.setImageResource(R.drawable.ic_star_black_24dp);
        }
        else if(item.getRated()==3){
            bintang1.setImageResource(R.drawable.ic_star_black_24dp);
            bintang2.setImageResource(R.drawable.ic_star_black_24dp);
            bintang3.setImageResource(R.drawable.ic_star_black_24dp);
        }
        else if(item.getRated()==2){
            bintang1.setImageResource(R.drawable.ic_star_black_24dp);
            bintang2.setImageResource(R.drawable.ic_star_black_24dp);
        }
        else if(item.getRated()==1){
            bintang1.setImageResource(R.drawable.ic_star_black_24dp);
        }

        if(item.isLiked()){
            like.setImageResource(R.drawable.ic_favorite);
        }
        else {
            like.setImageResource(R.drawable.ic_favorite_border);
        }

        webView= findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClient());
        webView.loadUrl("https://www.youtube.com/embed?listType=search&list="+item.getTitle());
    }

    public void back(View view) {
        Intent i = new Intent(DetailResepActivity.this,MainActivity.class);
        startActivity(i);
    }

    public void like(View view) {
        if(item.isLiked()){
            final String userid = LoginActivity.account.getId();
            final String url = "http://furniterior.000webhostapp.com/mdp/ws/delete_like.php";
            final StringRequest stringRequest = new StringRequest(
                    Request.Method.POST,
                    url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("userid", userid);
                    params.put("item", item.getJson());

                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);
            item.setLiked(false);
            like.setImageResource(R.drawable.ic_favorite_border);
        }else{
            final String userid = LoginActivity.account.getId();
            final String url = "http://furniterior.000webhostapp.com/mdp/ws/insert_like.php";
            final StringRequest stringRequest = new StringRequest(
                    Request.Method.POST,
                    url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("userid", userid);
                    params.put("itemid", item.getJson());

                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);
            item.setLiked(true);
            like.setImageResource(R.drawable.ic_favorite);
        }
    }

    public void share(View view) {
        Intent intent = new Intent(getApplicationContext(), ShareActivity.class);
        intent.putExtra("item", item);
        getApplication().startActivity(intent);
    }

    public void rate1(View view) {
        bintang1.setImageResource(R.drawable.ic_star_border);
        bintang2.setImageResource(R.drawable.ic_star_border);
        bintang3.setImageResource(R.drawable.ic_star_border);
        bintang4.setImageResource(R.drawable.ic_star_border);
        bintang5.setImageResource(R.drawable.ic_star_border);

        bintang1.setImageResource(R.drawable.ic_star_black_24dp);

        item.setRated(1);
        final String userid = LoginActivity.account.getId();
        final String url = "http://furniterior.000webhostapp.com/mdp/ws/insert_rate.php";
        final StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("userid", userid);
                params.put("itemid", item.getJson());
                params.put("rate", "1");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    public void rate2(View view) {
        bintang1.setImageResource(R.drawable.ic_star_border);
        bintang2.setImageResource(R.drawable.ic_star_border);
        bintang3.setImageResource(R.drawable.ic_star_border);
        bintang4.setImageResource(R.drawable.ic_star_border);
        bintang5.setImageResource(R.drawable.ic_star_border);

        bintang1.setImageResource(R.drawable.ic_star_black_24dp);
        bintang2.setImageResource(R.drawable.ic_star_black_24dp);

        item.setRated(2);
        final String userid = LoginActivity.account.getId();
        final String url = "http://furniterior.000webhostapp.com/mdp/ws/insert_rate.php";
        final StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("userid", userid);
                params.put("itemid", item.getJson());
                params.put("rate", "2");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    public void rate3(View view) {
        bintang1.setImageResource(R.drawable.ic_star_border);
        bintang2.setImageResource(R.drawable.ic_star_border);
        bintang3.setImageResource(R.drawable.ic_star_border);
        bintang4.setImageResource(R.drawable.ic_star_border);
        bintang5.setImageResource(R.drawable.ic_star_border);

        bintang1.setImageResource(R.drawable.ic_star_black_24dp);
        bintang2.setImageResource(R.drawable.ic_star_black_24dp);
        bintang3.setImageResource(R.drawable.ic_star_black_24dp);

        item.setRated(3);
        final String userid = LoginActivity.account.getId();
        final String url = "http://furniterior.000webhostapp.com/mdp/ws/insert_rate.php";
        final StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("userid", userid);
                params.put("itemid", item.getJson());
                params.put("rate", "3");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }
    public void rate4(View view) {
        bintang1.setImageResource(R.drawable.ic_star_border);
        bintang2.setImageResource(R.drawable.ic_star_border);
        bintang3.setImageResource(R.drawable.ic_star_border);
        bintang4.setImageResource(R.drawable.ic_star_border);
        bintang5.setImageResource(R.drawable.ic_star_border);

        bintang1.setImageResource(R.drawable.ic_star_black_24dp);
        bintang2.setImageResource(R.drawable.ic_star_black_24dp);
        bintang3.setImageResource(R.drawable.ic_star_black_24dp);
        bintang4.setImageResource(R.drawable.ic_star_black_24dp);

        item.setRated(4);
        final String userid = LoginActivity.account.getId();
        final String url = "http://furniterior.000webhostapp.com/mdp/ws/insert_rate.php";
        final StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("userid", userid);
                params.put("itemid", item.getJson());
                params.put("rate", "4");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }
    public void rate5(View view) {
        bintang1.setImageResource(R.drawable.ic_star_border);
        bintang2.setImageResource(R.drawable.ic_star_border);
        bintang3.setImageResource(R.drawable.ic_star_border);
        bintang4.setImageResource(R.drawable.ic_star_border);
        bintang5.setImageResource(R.drawable.ic_star_border);

        bintang1.setImageResource(R.drawable.ic_star_black_24dp);
        bintang2.setImageResource(R.drawable.ic_star_black_24dp);
        bintang3.setImageResource(R.drawable.ic_star_black_24dp);
        bintang4.setImageResource(R.drawable.ic_star_black_24dp);
        bintang5.setImageResource(R.drawable.ic_star_black_24dp);

        item.setRated(5);
        final String userid = LoginActivity.account.getId();
        final String url = "http://furniterior.000webhostapp.com/mdp/ws/insert_rate.php";
        final StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("userid", userid);
                params.put("itemid", item.getJson());
                params.put("rate", "5");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }
}
