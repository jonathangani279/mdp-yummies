package com.project.yummies;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

public class HomeFragment extends Fragment {
    RecyclerView rv;
    Adapter_rv_home adapterRvHome;
    ArrayList<Item> arrItem = new ArrayList<>();
    MainActivity m;
    public static JSONObject obj;
    RVRecipesClickListener RVRecipes;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_home, container, false);
        m = (MainActivity) getActivity();
        rv = v.findViewById(R.id.fragment_home_rv);
        adapterRvHome = new Adapter_rv_home(arrItem, new RVRecipesClickListener() {
            @Override
            public void recipesClickListener(View v, int posisi) {
                Item item = null;
                try {
                    item= new Item(new JSONObject(arrItem.get(posisi).getJson()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Intent i = new Intent(HomeFragment.this.getContext(),DetailResepActivity.class);
                i.putExtra("item",item);
                startActivity(i);
            }
        });

        //adapterRvHome.setHasStableIds(true);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(getContext()));
        rv.setAdapter(adapterRvHome);

        GradientDrawable drawable = new GradientDrawable();
        drawable.setShape(GradientDrawable.RECTANGLE);
        drawable.setCornerRadius(20);
        drawable.setColor(Color.WHITE);
        drawable.setStroke(5, Color.BLACK);
        rv.setBackground(drawable);

        final String url = "http://furniterior.000webhostapp.com/mdp/ws/get_home.php?userid="+LoginActivity.account.getId();
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            arrItem.clear();
                            obj = new JSONObject(response);
                            JSONArray list = obj.getJSONArray("results");
                            for (int i = 0; i< list.length(); i++){
                                JSONObject jsonItem = list.getJSONObject(i);
                                Item item = new Item(jsonItem);
                                arrItem.add(item);
                            }
                            adapterRvHome.notifyDataSetChanged();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        );

        RequestQueue requestQueue = Volley.newRequestQueue(m.getApplicationContext());
        requestQueue.add(stringRequest);
        return v;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}