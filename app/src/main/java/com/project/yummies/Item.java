package com.project.yummies;

import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class Item implements Serializable {
    private String id;
    private String title;
    private String ready;
    private String portion;
    private String imgurl;
    private String json;
    private int rated;
    private float rating;
    private boolean isBookmarked;
    private boolean isLiked;
    private ArrayList<String>step;
    public Item(JSONObject object) {
        try {
            this.id = object.getString("id");
            this.title = object.getString("title");
            this.ready = object.getString("readyInMinutes");
            this.portion = object.getString("servings");
            this.imgurl = object.getString("image");

            this.isBookmarked = object.getBoolean("bookmarked");
            this.isLiked = object.getBoolean("liked");
            this.step = new ArrayList<>();
            JSONArray analyzedInstructions = object.getJSONArray("analyzedInstructions");
            JSONObject prop = analyzedInstructions.getJSONObject(0);
            JSONArray steps = prop.getJSONArray("steps");
            for (int i = 0; i < steps.length(); i++) {
                JSONObject obj = steps.getJSONObject(i);
                this.step.add(obj.getString("step"));
            }
            this.rated = Integer.parseInt(object.getString("rated"));
            this.rating = (!object.getString("rating").equals("null") )?Float.parseFloat(object.getString("rating")) : 0;
            this.json = object.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getRated() {
        return rated;
    }

    public void setRated(int rated) {
        this.rated = rated;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public boolean isBookmarked() {
        return isBookmarked;
    }

    public void setBookmarked(boolean bookmarked) {
        isBookmarked = bookmarked;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public void setLiked(boolean liked) {
        isLiked = liked;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getReady() {
        return ready;
    }

    public void setReady(String ready) {
        this.ready = ready;
    }

    public String getPortion() {
        return portion;
    }

    public void setPortion(String portion) {
        this.portion = portion;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public ArrayList<String> getStep() {
        return step;
    }

    public void setStep(ArrayList<String> steps) {
        this.step = step;
    }
}
