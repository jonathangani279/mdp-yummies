package com.project.yummies;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class LikedFragment extends Fragment {
    RecyclerView rvLiked;
    ArrayList<Item> likedFood= new ArrayList<>();
    Adapter_rv_liked adapter;
    public static JSONObject obj;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_liked, container, false);
        rvLiked= view.findViewById(R.id.fragment_liked_rv);
        adapter= new Adapter_rv_liked(likedFood, new RVRecipesClickListener() {
            @Override
            public void recipesClickListener(View v, int posisi) {
                Item item = null;
                try {
                    item= new Item(new JSONObject(likedFood.get(posisi).getJson()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Intent i = new Intent(LikedFragment.this.getContext(),DetailResepActivity.class);
                i.putExtra("item",item);
                startActivity(i);
            }
        });

        //Toast.makeText(getContext(), LoginActivity.account.getId(), Toast.LENGTH_SHORT).show();
        //104082473564984161311

        rvLiked.setHasFixedSize(true);
        rvLiked.setLayoutManager(new LinearLayoutManager(getContext()));
        rvLiked.setAdapter(adapter);

        GradientDrawable drawable2 = new GradientDrawable();
        drawable2.setShape(GradientDrawable.RECTANGLE);
        drawable2.setCornerRadius(20);
        drawable2.setColor(Color.WHITE);
        drawable2.setStroke(5, Color.BLACK);
        rvLiked.setBackground(drawable2);

        getLikedFood();
        
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void getLikedFood() {
        String url = "http://furniterior.000webhostapp.com/mdp/ws/get_like.php?userid="+LoginActivity.account.getId();
        StringRequest request= new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    likedFood.clear();
                    obj = new JSONObject(response);
                    JSONArray list = obj.getJSONArray("results");

                    for (int i = 0; i< list.length(); i++){
                        JSONObject jsonItem = list.getJSONObject(i);
                        Item item = new Item(jsonItem);
                        likedFood.add(item);
                    }

                    adapter.notifyDataSetChanged();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getContext().getApplicationContext());
        requestQueue.add(request);
    }
}
