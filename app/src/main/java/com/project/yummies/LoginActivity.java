package com.project.yummies;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {
    SignInButton btnSignIn;
    GoogleSignInOptions signInOptions;
    public static GoogleSignInClient signInClient;
    public static GoogleSignInAccount account;
    int RC_SIGN_IN= 0; //REQ CODE

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnSignIn= (SignInButton)findViewById(R.id.btnSignIn);
        btnSignIn.setSize(SignInButton.SIZE_WIDE);
        signInOptions= new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        signInClient= GoogleSignIn.getClient(this, signInOptions);

        btnSignIn.setOnClickListener(onClickSignIn);
    }

    @Override
    protected void onStart() { //CEK SUDAH ADA YG LOGIN ATAU BELUM
        super.onStart();

        account = GoogleSignIn.getLastSignedInAccount(this);
        //JIKA ADA MAKA MASUK KE HOME
        if (account != null) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //HASIL DARI INTENT GOOGLE SIGN IN CLIENT
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private View.OnClickListener onClickSignIn= new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            startActivityForResult(signInClient.getSignInIntent(), RC_SIGN_IN);
        }
    };

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            completedTask.getResult(ApiException.class);
            account = GoogleSignIn.getLastSignedInAccount(this);
            String userid = account.getId();
            String namadepan = account.getGivenName();
            String namabelakang = account.getFamilyName();
            String pp = (account.getPhotoUrl() != null )?account.getPhotoUrl().toString() : "";
            final String url = "http://furniterior.000webhostapp.com/mdp/ws/insert_user.php?userid="+userid+"&namadepan="+namadepan+"&namabelakang="+namabelakang+"&pp="+pp;
            System.out.println(url);
            StringRequest stringRequest = new StringRequest(
                    Request.Method.GET,
                    url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }
            );

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
        } catch (ApiException e) {
            Log.w("error", "signInResult:failed code=" + e.getStatusCode());
        }
    }
}
