package com.project.yummies;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    FrameLayout searchBar;
    ImageView ivBack;
    EditText edtSearch;
    ImageView ivAccount;
    BottomNavigationView bottomNav;
    public static GoogleSignInAccount account;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        System.out.println(LoginActivity.account.getAccount().toString());
        searchBar= findViewById(R.id.searchBar);
        ivBack= findViewById(R.id.ivBack);
        edtSearch= findViewById(R.id.edtSearch);
        ivAccount= findViewById(R.id.ivAccount);
        bottomNav= findViewById(R.id.bottomNav);
        account= GoogleSignIn.getLastSignedInAccount(this);

        searchBar.setOnClickListener(onClickSearchBar);
        ivAccount.setOnClickListener(onClickProfileImage);
        bottomNav.setOnNavigationItemSelectedListener(onItemSelectedNavigation);
        if (account != null) {
            Glide.with(this)
                    .load(account.getPhotoUrl())
                    .apply(RequestOptions.circleCropTransform())
                    .into(ivAccount);
        }

        showFragment(new HomeFragment());
    }

    private View.OnClickListener onClickProfileImage= new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            startActivity(new Intent(MainActivity.this, ProfileActivity.class));
        }
    };

    private View.OnClickListener onClickSearchBar= new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            toggleSearchBar("open");
            showFragment(new SearchFragment());
        }
    };

    private View.OnClickListener onClickBack= new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            toggleSearchBar("close");
            showFragment(new HomeFragment());
        }
    };

    private BottomNavigationView.OnNavigationItemSelectedListener onItemSelectedNavigation= new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.menuHome:
                    showFragment(new HomeFragment());
                    break;
                case R.id.menuRated:
                    showFragment(new RatedFragment());
                    break;
                case R.id.menuCommunity:
                    showFragment(new CommunityFragment());
                    break;
                case R.id.menuLiked:
                    showFragment(new LikedFragment());
                    break;
            }

            return true;
        }
    };

    private void showFragment(Fragment fragment) {
        FragmentTransaction trans= getSupportFragmentManager().beginTransaction();

        trans.replace(R.id.flContainer, fragment);
        trans.commit();
    }

    private void toggleSearchBar(String mode) {
        final TextView tvSearch= findViewById(R.id.tvSearch);
        InputMethodManager keyboard= (InputMethodManager) getSystemService(MainActivity.this.INPUT_METHOD_SERVICE);
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                SearchFragment.search(edtSearch.getText().toString(),HomeFragment.obj);
            }
        });

        if (mode.equals("open")) {
            tvSearch.setVisibility(View.INVISIBLE);
            ivBack.setImageResource(R.drawable.back);
            ivBack.setOnClickListener(onClickBack);
            edtSearch.setVisibility(View.VISIBLE);
            keyboard.showSoftInput(edtSearch, InputMethodManager.SHOW_IMPLICIT);
        } else if (mode.equals("close")) {
            tvSearch.setVisibility(View.VISIBLE);
            ivBack.setImageResource(R.drawable.search);
            ivBack.setOnClickListener(null);
            edtSearch.setVisibility(View.INVISIBLE);
            keyboard.hideSoftInputFromWindow(edtSearch.getWindowToken(), 0);
        }
    }
}
