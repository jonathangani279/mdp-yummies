package com.project.yummies;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ProfileActivity extends AppCompatActivity {
    Uri selected;
    EditText namadepan,namabelakang,username;
    TextView email,lingkaran;
    Button update;
    ImageView fotoprofil,back;
    int mode=0;
    int boleh=0;
    String strusername;
    String strnamadepan;
    String strnamabelakang;
    String stremail ;
    User user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
//        btnSignOut.setOnClickListener(onClickSignOut);
//
//        if (MainActivity.account != null) {
//            Glide.with(this)
//                    .load(MainActivity.account.getPhotoUrl())
//                    .apply(RequestOptions.circleCropTransform())
//                    .into(ivProfile);
//            tvName.setText(MainActivity.account.getDisplayName());
//        }
        namadepan=findViewById(R.id.etNamaDepan);
        namabelakang=findViewById(R.id.etNamaBelakang);
        username=findViewById(R.id.etUsername);
        email=findViewById(R.id.tvIsiIEmail);
        update=findViewById(R.id.btnGantiProfile);
        fotoprofil=findViewById(R.id.ivFotoProfil);
        back=findViewById(R.id.ivBack);
        lingkaran=findViewById(R.id.tvLingkaran);

        GradientDrawable drawable = new GradientDrawable();
        drawable.setShape(GradientDrawable.RECTANGLE);
        drawable.setCornerRadius(20);
        drawable.setColor(Color.WHITE);
        drawable.setStroke(1, Color.GRAY);
        namadepan.setBackground(drawable);

        GradientDrawable drawable2 = new GradientDrawable();
        drawable2.setShape(GradientDrawable.RECTANGLE);
        drawable2.setCornerRadius(20);
        drawable2.setColor(Color.WHITE);
        drawable2.setStroke(1, Color.GRAY);
        namabelakang.setBackground(drawable2);

        GradientDrawable drawable3 = new GradientDrawable();
        drawable3.setShape(GradientDrawable.RECTANGLE);
        drawable3.setCornerRadius(20);
        drawable3.setColor(Color.WHITE);
        drawable3.setStroke(1, Color.GRAY);
        username.setBackground(drawable3);

        GradientDrawable drawable4 = new GradientDrawable();
        drawable4.setShape(GradientDrawable.RECTANGLE);
        drawable4.setCornerRadius(100);
        drawable4.setColor(Color.BLACK);
        update.setBackground(drawable4);

        GradientDrawable drawable5 = new GradientDrawable();
        drawable5.setShape(GradientDrawable.OVAL);
        drawable5.setCornerRadius(100);
        drawable5.setStroke(5, Color.WHITE);
        lingkaran.setBackground(drawable5);

        String userid = LoginActivity.account.getId();
        final String url = "http://furniterior.000webhostapp.com/mdp/ws/get_user.php?userid="+userid;
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            user = new User(obj);
                            username.setText(user.getUser_username());
                            namadepan.setText(user.getUser_nama_depan());
                            namabelakang.setText(user.getUser_nama_belakang());
                            email.setText(LoginActivity.account.getEmail());
                            System.out.println(user.getUser_profile_picture());
                            Glide.with(getApplicationContext())
                                    .load(user.getUser_profile_picture())
                                    .apply(new RequestOptions().override(200,200))
                                    .into(fotoprofil);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }
        );

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);

        namadepan.setClickable(false);
        namadepan.setEnabled(false);
        namabelakang.setClickable(false);
        namabelakang.setEnabled(false);
        username.setClickable(false);
        username.setEnabled(false);
        fotoprofil.setClickable(false);
        fotoprofil.setEnabled(false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1 && data!=null){
            selected = data.getData();
            try {
                Bitmap bitmap=MediaStore.Images.Media.getBitmap(this.getContentResolver(), selected);
                RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), bitmap);

                circularBitmapDrawable.setCircular(true);
                fotoprofil.setImageDrawable(circularBitmapDrawable);
            } catch (IOException e) {

            }
        }
    }

    public void gantifoto(View view) {
        Intent change = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(change,1);
    }

    public void signout(View view) {
        if(username.getText().toString().equals("") && mode==1){
            boleh=0;
        }
        else if(!username.getText().toString().equals("")&&mode==0){
            boleh=1;
        }

        if(boleh==0){
            Toast.makeText(this, "Isi username anda terlebih dahulu", Toast.LENGTH_SHORT).show();
        }
        else{
            LoginActivity.signInClient
                    .signOut()
                    .addOnCompleteListener(ProfileActivity.this, new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            startActivity(new Intent(ProfileActivity.this, LoginActivity.class));
                        }
                    });
        }
    }

    public void back(View view) {
        if(username.getText().toString().equals("") && mode==1){
            boleh=0;
        }
        else if(!username.getText().toString().equals("")&&mode==0){
            boleh=1;
        }

        if(boleh==0){
            Toast.makeText(this, "Isi username anda terlebih dahulu", Toast.LENGTH_SHORT).show();
        }
        else{
            this.finish();
        }
    }

    public void update(View view) {
        if(mode==0){
            mode=1;
            namadepan.setClickable(true);
            namadepan.setEnabled(true);
            namabelakang.setClickable(true);
            namabelakang.setEnabled(true);
            username.setClickable(true);
            username.setEnabled(true);
            fotoprofil.setClickable(true);
            fotoprofil.setEnabled(true);

            Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.ic_save_black_24dp);
            update.setCompoundDrawablesWithIntrinsicBounds(img,null,null,null);
            update.setText("Save Changes");
        }
        else if(mode==1){
            strnamabelakang=namabelakang.getText().toString();
            strnamadepan=namadepan.getText().toString();
            strusername=username.getText().toString();

            if(strnamadepan.equals("")){
                Toast.makeText(this, "Nama depan tidak boleh kosong", Toast.LENGTH_SHORT).show();
            }
            else if(strusername.equals("")){
                Toast.makeText(this, "Username tidak boleh kosong", Toast.LENGTH_SHORT).show();
            }
            else if(strusername.length()<8 || strusername.length()>16){
                Toast.makeText(this, "Panjang username antara 8-16 karakter", Toast.LENGTH_SHORT).show();
            }
            else {
                mode = 0;
                namadepan.setClickable(false);
                namadepan.setEnabled(false);
                namabelakang.setClickable(false);
                namabelakang.setEnabled(false);
                username.setClickable(false);
                username.setEnabled(false);
                fotoprofil.setClickable(false);
                fotoprofil.setEnabled(false);

                Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.ic_edit_black_24dp);
                update.setCompoundDrawablesWithIntrinsicBounds(img,null,null,null);
                update.setText("Update Profile");
                Toast.makeText(this, "Profile anda berhasil di update", Toast.LENGTH_SHORT).show();

                String uploadImage="";
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selected);
                    uploadImage = getStringImage(bitmap);
                }catch (Exception e){

                }
                final String userid = LoginActivity.account.getId();
                final String url = "http://furniterior.000webhostapp.com/mdp/ws/update_user.php";
                final String finalUploadImage = uploadImage;
                final StringRequest stringRequest = new StringRequest(
                        Request.Method.POST,
                        url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                System.out.println(finalUploadImage);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("userid", userid);
                        params.put("depan", namadepan.getText().toString());
                        params.put("belakang", namabelakang.getText().toString());
                        params.put("username", username.getText().toString());
                        params.put("img", finalUploadImage);
                        return params;
                    }
                };

                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                requestQueue.add(stringRequest);
                boleh = 1;
				
            }
        }
    }

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }
}
