package com.project.yummies;

import android.view.View;

public interface RVRecipesClickListener {
    public void recipesClickListener(View v, int posisi);
}
