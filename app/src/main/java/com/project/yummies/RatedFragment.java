package com.project.yummies;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

public class RatedFragment extends Fragment {
    RecyclerView rvRated;
    ArrayList<Item> ratedFood= new ArrayList<>();
    Adapter_rv_rated adapter;
    public static JSONObject obj;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rated, container, false);
        rvRated= view.findViewById(R.id.fragment_rated_rv);
        adapter= new Adapter_rv_rated(ratedFood, new RVRecipesClickListener() {
            @Override
            public void recipesClickListener(View v, int posisi) {
                Item item = null;

                try {
                    item= new Item(new JSONObject(ratedFood.get(posisi).getJson()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Intent i = new Intent(RatedFragment.this.getContext(),DetailResepActivity.class);
                i.putExtra("item",item);
                startActivity(i);
            }
        });

        //104082473564984161311

        rvRated.setHasFixedSize(true);
        rvRated.setLayoutManager(new LinearLayoutManager(getContext()));
        rvRated.setAdapter(adapter);

        GradientDrawable drawable2 = new GradientDrawable();
        drawable2.setShape(GradientDrawable.RECTANGLE);
        drawable2.setCornerRadius(20);
        drawable2.setColor(Color.WHITE);
        drawable2.setStroke(5, Color.BLACK);
        rvRated.setBackground(drawable2);

        getRatedFood();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void getRatedFood() {
        String url= "http://furniterior.000webhostapp.com/mdp/ws/get_rated.php?userid="+LoginActivity.account.getId();
        StringRequest request= new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    ratedFood.clear();
                    obj = new JSONObject(response);
                    JSONArray list = obj.getJSONArray("results");

                    for (int i = 0; i< list.length(); i++){
                        JSONObject jsonItem = list.getJSONObject(i);
                        Item item = new Item(jsonItem);
                        ratedFood.add(item);
                    }

                    adapter.notifyDataSetChanged();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getContext().getApplicationContext());
        requestQueue.add(request);
    }
}
