package com.project.yummies;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SearchFragment extends Fragment {
    TabLayout tlSearch;
    EditText edSearch;
    RecyclerView rvsearch;
    public static Adapter_rv_home adapterRvSearch;
    public static ArrayList<Item> arrItem = new ArrayList<>();
    public static Adapter_rv_user adapterRvUser;
    public static ArrayList<User> arrUser = new ArrayList<>();
    public static int position=0;
    static MainActivity m;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        m = (MainActivity) getActivity();
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tlSearch= view.findViewById(R.id.tlSearch);
        tlSearch.addOnTabSelectedListener(onTabSelected);
        rvsearch = view.findViewById(R.id.recyclerview2);
        adapterRvSearch= new Adapter_rv_home(arrItem);
        adapterRvUser=new Adapter_rv_user(arrUser);
        //adapterRvSearch.setHasStableIds(true);
        rvsearch.setHasFixedSize(true);
        rvsearch.setLayoutManager(new LinearLayoutManager(getContext()));
        rvsearch.setAdapter(adapterRvSearch);
    }

    private TabLayout.OnTabSelectedListener onTabSelected= new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            if (tab.getPosition() == 0) {
                position = 0;
                adapterRvSearch= new Adapter_rv_home(arrItem);

                //adapterRvSearch.setHasStableIds(true);
                rvsearch.setHasFixedSize(true);
                rvsearch.setLayoutManager(new LinearLayoutManager(getContext()));
                rvsearch.setAdapter(adapterRvSearch);
            } else if (tab.getPosition() == 1) {
                position = 1;
                adapterRvUser= new Adapter_rv_user(arrUser);

                //adapterRvUser.setHasStableIds(true);
                rvsearch.setHasFixedSize(true);
                rvsearch.setLayoutManager(new LinearLayoutManager(getContext()));
                rvsearch.setAdapter(adapterRvUser);
            }
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {
            if (tab.getPosition() == 0) {
                position = 0;
                adapterRvSearch= new Adapter_rv_home(arrItem);

                //adapterRvSearch.setHasStableIds(true);
                rvsearch.setHasFixedSize(true);
                rvsearch.setLayoutManager(new LinearLayoutManager(getContext()));
                rvsearch.setAdapter(adapterRvSearch);
            } else if (tab.getPosition() == 1) {
                position = 1;
                adapterRvUser= new Adapter_rv_user(arrUser);

                //adapterRvUser.setHasStableIds(true);
                rvsearch.setHasFixedSize(true);
                rvsearch.setLayoutManager(new LinearLayoutManager(getContext()));
                rvsearch.setAdapter(adapterRvUser);
            }
        }
    };
    public static void search(String text,JSONObject obj2)
    {
        if(!text.equals("") && position == 0 ){
            arrItem.clear();
            try {
                JSONObject obj = obj2;
                JSONArray list = obj.getJSONArray("results");
                if(!text.equals(""))
                {
                    for (int i = 0; i< list.length(); i++){
                        JSONObject jsonItem = list.getJSONObject(i);
                        Item item = new Item(jsonItem);
                        if(item.getTitle().toLowerCase().contains(text.toString()))
                        {
                            System.out.println("ada");
                            arrItem.add(item);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            adapterRvSearch.notifyDataSetChanged();
        }else if(!text.equals("") && position == 1 ){
            final String url = "http://furniterior.000webhostapp.com/mdp/ws/get_users.php?userid="+LoginActivity.account.getId()+"&username="+text;
            StringRequest stringRequest = new StringRequest(
                    Request.Method.GET,
                    url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject obj = new JSONObject(response);
                                JSONArray list = obj.getJSONArray("results");
                                arrUser.clear();
                                for (int i = 0; i < list.length(); i++) {
                                    JSONObject jsonItem = list.getJSONObject(i);
                                    arrUser.add(new User(jsonItem));
                                }
                                adapterRvUser.notifyDataSetChanged();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }
            );

            RequestQueue requestQueue = Volley.newRequestQueue(m.getApplicationContext());
            requestQueue.add(stringRequest);
        }else{
            arrItem.clear();
            arrUser.clear();
            adapterRvSearch.notifyDataSetChanged();
            adapterRvUser.notifyDataSetChanged();
        }
    }
}
