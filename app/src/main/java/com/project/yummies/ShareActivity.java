package com.project.yummies;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ShareActivity extends AppCompatActivity {

    ArrayList<User> arrUser = new ArrayList<>();
    RecyclerView rv;
    Button btnDone;
    Adapter_rv_share adapter;
    public static Item selected;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);
        rv = findViewById(R.id.share_rv_user);
        selected = (Item) getIntent().getSerializableExtra("item");
        btnDone = findViewById(R.id.share_btn_done);
        adapter = new Adapter_rv_share(arrUser);
        loadFollowing();
        adapter.setHasStableIds(true);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(adapter);

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void loadFollowing(){
        final String url = "http://furniterior.000webhostapp.com/mdp/ws/get_following.php?userid="+LoginActivity.account.getId();
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray list = obj.getJSONArray("results");
                            arrUser.clear();
                            for (int i = 0; i < list.length(); i++) {
                                JSONObject jsonItem = list.getJSONObject(i);
                                arrUser.add(new User(jsonItem));
                            }
                            adapter.notifyDataSetChanged();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        );

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
