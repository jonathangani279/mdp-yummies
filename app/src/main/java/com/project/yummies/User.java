package com.project.yummies;

import android.net.Uri;

import org.json.JSONException;
import org.json.JSONObject;

public class User {
    private String user_id;
    private String user_nama_depan;
    private String user_nama_belakang;
    private String user_username;
    private String tulisan;
    private String user_email;
    private String user_profile_picture;

    public User(JSONObject object) {
        try {
            this.user_id = object.getString("user_id");
            this.user_nama_depan = object.getString("user_nama_depan");
            this.user_nama_belakang = object.getString("user_nama_belakang");
            this.user_username = (object.getString("user_username").equals("null") || object.getString("user_username") == null )? "" :object.getString("user_username");
            this.user_profile_picture = (object.getString("user_profile_picture").equals(""))? "http://furniterior.000webhostapp.com/projek/p/default.png":object.getString("user_profile_picture");
            this.tulisan = object.getString("tulisan");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getTulisan() {
        return tulisan;
    }

    public void setTulisan(String tulisan) {
        this.tulisan = tulisan;
	}
	public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_nama_depan() {
        return user_nama_depan;
    }

    public void setUser_nama_depan(String user_nama_depan) {
        this.user_nama_depan = user_nama_depan;
    }

    public String getUser_nama_belakang() {
        return user_nama_belakang;
    }

    public void setUser_nama_belakang(String user_nama_belakang) {
        this.user_nama_belakang = user_nama_belakang;
    }

    public String getUser_username() {
        return user_username;
    }

    public void setUser_username(String user_username) {
        this.user_username = user_username;
    }

    public String getUser_profile_picture() {
        return user_profile_picture;
    }

    public void setUser_profile_picture(String user_profile_picture) {
        this.user_profile_picture = user_profile_picture;
    }
}
